#!/bin/sh
ARG1=${1:-message}
ARG2=${2:-None}
if [ $ARG1 = "message" ]; then
echo "Usage: client [Game server IP address] [OPTIONAL: Observer IP address (Game Server IP used as default if omitted)]"
else
cd cannonball
if [ $ARG2 = "None" ]; then
sudo ./SNESclient $ARG1 &
else
sudo ./SNESclient $ARG1 $ARG2 &
fi
cd ..
fi
