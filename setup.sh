#!/bin/sh
sudo apt-get update
#disable the automatic startup of the Emulation Station frontend on bootup of RetroPie
if [ -f "/opt/retropie/configs/all/autostart.sh" ]; then
sed -i 's/emulationstation/#emulationstation/g' /opt/retropie/configs/all/autostart.sh
else
#Not RetroPie so install the SDL2 library
sudo mv ./libSDL2-2.0.so.0 /usr/lib/arm-linux-gnueabihf
sudo mv ./libSDL2-2.0.so.0.2.1 /usr/lib/arm-linux-gnueabihf
sudo mv ./libSDL2.a /usr/lib/arm-linux-gnueabihf
sudo mv ./libSDL2.la /usr/lib/arm-linux-gnueabihf
sudo mv ./libSDL2.so /usr/lib/arm-linux-gnueabihf
sudo mv ./libSDL2_test.a /usr/lib/arm-linux-gnueabihf
sudo mv ./libSDL2main.a /usr/lib/arm-linux-gnueabihf
fi
mv server.sh ~/
mv client.sh ~/
mv cannonball.sh ~/
mv observer.sh ~/
chmod +x cannonball
chmod +x SNESserver
chmod +x SNESclient
chmod +x SNESobserver
cd ~/
chmod +x server.sh
chmod +x client.sh
chmod +x cannonball.sh
chmod +x observer.sh
cd ~/
echo "Setup complete..."
