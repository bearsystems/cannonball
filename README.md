Cannonball - OutRun Engine
==========================

See [Reassembler Blog](http://reassembler.blogspot.co.uk/).

Credits
-------

* Chris White - Project creator.
* Arun Horne  - Cross platform work.

Notes
-----
*This release has been modified by Bear Systems to allow the controller to be attached to a remote computer and run the game over an IP connection.
*To launch in this configuration, connect the controller to the remote computer and run the SNESclient [IP address of the game computer]. Then use ./cannonball to launch the game

Getting Started
---------------

There are four executables in the package:
* cannonball
  	This is the Game engine which requires a display connected to the HDMI port on the Raspberry Pi and a USB Keyboard to launch the program
* SNESserver
  	This is a test utility to verify that the IP connection between two Raspberry Pi's is working properly it runs on the same Raspberry Pi as the cannonball game
* SNESclient
	This is the application that runs on the Raspberry Pi with a SNES controller attached to the USB port
* SNESobserver
        This is the application that receives clear text representations of the controller events sent to the game. It can run on the same Raspberry Pi as the cannonball game or on a third Raspberry Pi
	
Hardware Requirements
---------------------

* Two Raspberry Pi's connected to a network and visible to each other by a ping command
* One SNES USB game controller connected to one Raspberry Pi's USB port
* Optional: A monitor and Keyboard connected to the game controller Raspberry Pi. 
	(Alternatively, an SSH terminal into this Raspberry Pi will suffice)
* One Monitor connected to the other Raspberry Pi's HDMI connection
* One USB Keyboard connected to the same Raspberry Pi as the monitor
	
Testing the Connection
----------------------

*Boot both Raspberry Pi's
*On the Raspberry Pi that has the monitor and keyboard attached, run the following command from the ~/ directory:
	ifconfig
	Note the eth0 inet addr value
*On the Raspberry Pi, that has the controller attached, run the following command from the ~/ directory:
	./client.sh <IP address of the Raspberry Pi with the Game> 
	
*On the Raspberry Pi that is connected to the Monitor run the following command from the ~/ directory:
	./server.sh
	
*Operate the buttons on the controller and verify that output appears on the screen as the example below:
	
	Client Pi:
	A Button Pressed
	A Button Released
	B Button Pressed
	B Button Released
	Down Button Pressed
	Right Button Pressed
	Horizontal Button Released
	Vertical Button Released
	
	Server Pi:
	SDL_JOYBUTTONUP 1
	SDL_JOYBUTTONDOWN 2
	SDL_JOYBUTTONUP 2
	SDL_JOYAXISMOTION 1
	SDL_JOYAXISMOTION 0
	SDL_JOYAXISMOTION 0
	SDL_JOYAXISMOTION 1
	
*Hold the two shoulder buttons (the ones on the top of the controller) and press the 'Select' button to exit both applications
	
Running the Game
----------------
*Boot both Raspberry Pi's

*On the Raspberry Pi that has the monitor and keyboard attached, run the following command from the ~/ directory:
	ifconfig
	Note the eth0 inet addr value

*On the Raspberry Pi that has the controller attached, run the following command from the ~/ directory:
        ./client.sh <IP address of the Raspberry Pi with the monitor and keyboard attached > <OPTIONAL: IP address of the Raspberry Pi that will run the Observer program>
        Note: the second argument is only necessary when the Observer program is run on a different Raspberry Pi from the game.

*On the Raspberry Pi that is connected to the Monitor, run the following command from the ~/ directory:
	./cannonball.sh 

*On the Raspberry Pi that is to be used for the observer, open an SSH terminal from a PC and run the following command from the ~/ directory:
        ./observer.sh
 


*The game may be exited from the menu using the controller. This does not shutdown the control client or the observer

*The game may also be exited by holding both shoulder buttons and pressing the 'Select' button. This shuts down the game, the observer, and the control client application on the client Pi.